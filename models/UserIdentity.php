<?php
namespace app_bis_sekolah_api\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 *
 * @property integer $id
 * @property string $username
 */
class UserIdentity extends \technosmart\models\User
{

    public function getUser()
    {
        return $this->hasOne(\app_bis_sekolah_admin\models\User::className(), ['id' => 'id']);
    }
}