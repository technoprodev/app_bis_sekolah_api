<?php
$params['user.passwordResetTokenExpire'] = 3600;
$params['allowedOrigins'] = ['*'];

$config = [
    'id' => 'app_bis_sekolah_api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_bis_sekolah_api\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app_bis_sekolah_api/modules/v1',
            'class' => 'app_bis_sekolah_api\modules\v1\Module',
        ],
    ],
    'components' => [
        'request' => [
            // 'csrfParam' => '_csrf-app_bis_sekolah_api',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityClass' => 'app_bis_sekolah_api\models\UserIdentity',
            'enableAutoLogin' => false,
            'enableSession' => false,
            'loginUrl' => null
            // 'identityCookie' => ['name' => '_identity-app_bis_sekolah_api', 'httpOnly' => true],
        ],
        /*'session' => [
            'name' => 'session-app_bis_sekolah_api',
        ],*/
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        /*'errorHandler' => [
            'errorAction' => 'site/error',
        ],*/
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [ // AuthController
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/auth',
                    'pluralize' => false,
                    'patterns' => [
                        'GET login' => 'login',
                        'POST signup' => 'signup',
                        'POST resend-verification-email' => 'resend-verification-email',
                        'POST request-reset-password' => 'request-reset-password',
                        'PATCH verify' => 'verify-email',
                        'PATCH update-password' => 'update-password',
                        'OPTIONS' => 'options',
                    ]
                ],
                [ // BusController
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/bus',
                    'pluralize' => false,
                    'patterns' => [
                        'POST,HEAD index' => 'index',
                        'POST create' => 'create',
                        'PUT,PATCH update/<id>' => 'update',
                        'DELETE delete/<id>' => 'delete',
                        'OPTIONS' => 'options',
                    ]
                ],
                [ // DriverController
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/driver',
                    'pluralize' => false,
                    'patterns' => [
                        'GET,HEAD index' => 'index',
                        'GET,HEAD index/<id>' => 'index',
                        'POST create' => 'create',
                        'PUT,PATCH update/<id>' => 'update',
                        'DELETE delete/<id>' => 'delete',
                        'OPTIONS' => 'options',
                    ]
                ],
                [ // BusTrackingController
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/bus-tracking',
                    'pluralize' => false,
                    'patterns' => [
                        'GET,HEAD index' => 'index',
                        'GET,HEAD index/<id>' => 'index',
                        'POST create' => 'create',
                        'PUT,PATCH update/<id>' => 'update',
                        'DELETE delete/<id>' => 'delete',
                        'OPTIONS' => 'options',
                    ]
                ],
                [ // DriverAttendanceController
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/driver-attendance',
                    'pluralize' => false,
                    'patterns' => [
                        'GET,HEAD index' => 'index',
                        'GET,HEAD index/<id>' => 'index',
                        'POST create' => 'create',
                        'PUT,PATCH update/<id>' => 'update',
                        'DELETE delete/<id>' => 'delete',
                        'OPTIONS' => 'options',
                    ]
                ],
                [ // StudentAttendanceController
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/student-attendance',
                    'pluralize' => false,
                    'patterns' => [
                        'POST,HEAD index' => 'index',
                        'POST create' => 'create',
                        'PUT,PATCH update/<id>' => 'update',
                        'DELETE delete/<id>' => 'delete',
                        'OPTIONS' => 'options',
                    ]
                ],
            ],
        ],
    ],
    'params' => $params,
];

return $config;