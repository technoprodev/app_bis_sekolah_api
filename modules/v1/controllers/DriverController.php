<?php
namespace app_bis_sekolah_api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use app_bis_sekolah_admin\models\UserIdentity;
use app_bis_sekolah_admin\models\Driver;
use yii\widgets\ActiveForm;

/**
 * DriverController implements highly advanced CRUD actions for Driver model.
 */
class DriverController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter for cors to work
        unset($behaviors['authenticator']);

        // Allow XHR Requests from our different subdomains and dev machines
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        // Bearer Auth checks for Authorize: Bearer <Token> header to login the user
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    public function actionIndex($id = null)
    {
        // view all data
        if (!$id)
            return [
                "status" => "success",
                "data" => Driver::find()->asArray()->all(),
            ];
        
        // view single data
        $model['driver'] = $this->findModel($id);
        return [
            "status" => "success",
            "data" => Driver::find($id)->asArray()->one(),
        ];
    }

    public function actionCreate()
    {
        $error = false;

        $model['user'] = isset($id) ? $this->findModelUserIdentity($id) : new UserIdentity();
        $model['user']->scenario = 'repass';
        $model['driver'] = isset($id) ? $this->findModel($id) : new Driver();

        $post = Yii::$app->request->post();

        $model['user']->load($post);
        $model['driver']->load($post);

        if ($model['user']->isNewRecord) {
            $model['user']->status = 1;
        }
        $model['driver']->name = $model['user']->name;

        $transaction['user'] = UserIdentity::getDb()->beginTransaction();
        $transaction['driver'] = Driver::getDb()->beginTransaction();

        try {
            if (!$model['user']->save()) {
                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
            }

            $model['driver']->id = $model['user']->id;
            if (!$model['driver']->save()) {
                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
            }
            
            $transaction['user']->commit();
            $transaction['driver']->commit();
        } catch (\Exception $e) {
            $render = true;
            $transaction['user']->rollBack();
            $transaction['driver']->rollBack();
        } catch (\Throwable $e) {
            $render = true;
            $transaction['user']->rollBack();
            $transaction['driver']->rollBack();
        }

        if ($error)
            if (isset($model['driver']->errors))
                return [
                    "status" => "fail",
                    "data" => $model['driver']->errors
                ];
            else
                return [
                    "status" => "error",
                    "message" => "We've faced a problem creating the bus, please contact us for assistance."
                ];
        else
            return [
                "status" => "success",
                "data" => "Driver created successfully",
            ];
    }

    public function actionUpdate($id)
    {
        $error = false;

        $model['user'] = isset($id) ? $this->findModelUserIdentity($id) : new UserIdentity();
        $model['user']->scenario = 'repass';
        $model['driver'] = isset($id) ? $this->findModel($id) : new Driver();

        $post = Yii::$app->request->post();

        $model['driver']->load($post);

        $transaction['driver'] = Driver::getDb()->beginTransaction();

        try {
            if ($model['driver']->isNewRecord) {}
            if (!$model['driver']->save()) {
                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
            }
            
            $transaction['driver']->commit();
        } catch (\Exception $e) {
            $error = true;
            $transaction['driver']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['driver']->rollBack();
        }

        if ($error)
            if (isset($model['driver']->errors))
                return [
                    "status" => "fail",
                    "data" => $model['driver']->errors
                ];
            else
                return [
                    "status" => "error",
                    "message" => "We've faced a problem updating the bus, please contact us for assistance."
                ];
        else
            return [
                "status" => "success",
                "data" => "Driver updated successfully",
            ];
    }

    /**
     * Finds the Driver model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Driver the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Driver::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the UserIdentity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserIdentity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelUserIdentity($id)
    {
        if (($model = UserIdentity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
