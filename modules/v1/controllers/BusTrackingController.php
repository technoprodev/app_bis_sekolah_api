<?php

namespace app_bis_sekolah_api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use app_bis_sekolah_admin\models\BusTracking; //Nama Model yang perlu di ketehui utk memangil api
use yii\widgets\ActiveForm;

class BusTrackingController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter for cors to work
        unset($behaviors['authenticator']);

        // Allow XHR Requests from our different subdomains and dev machines
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        // Bearer Auth checks for Authorize: Bearer <Token> header to login the user
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    public function actionIndex($id = null)
    {
        // view all data
        if (!$id)
            return [
                "status" => "success",
                "data" => BusTracking::find()->asArray()->all(),
            ];
        
        // view single data
        $model['busTracking'] = $this->findModel($id);
        return [
            "status" => "success",
            "data" => BusTracking::find($id)->asArray()->one(),
        ];
    }

    public function actionCreate()
    {
        $error = false;

        $model['busTracking'] = isset($id) ? $this->findModel($id) : new BusTracking();

        $post = Yii::$app->request->post();

        $model['busTracking']->load($post);

        $transaction['busTracking'] = BusTracking::getDb()->beginTransaction();

        try {
            if ($model['busTracking']->isNewRecord) {}
            if (!$model['busTracking']->save()) {
                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
            }
            
            $transaction['busTracking']->commit();
        } catch (\Exception $e) {
            $error = true;
            $transaction['busTracking']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['busTracking']->rollBack();
        }

        if ($error)
            if (isset($model['busTracking']->errors))
                return [
                    "status" => "fail",
                    "data" => $model['busTracking']->errors
                ];
            else
                return [
                    "status" => "error",
                    "message" => "We've faced a problem creating the bus, please contact us for assistance."
                ];
        else
            return [
                "status" => "success",
                "data" => "Bus Tracking created successfully",
            ];
    }

    public function actionUpdate($id)
    {
        $error = false;

        $model['busTracking'] = isset($id) ? $this->findModel($id) : new BusTracking();

        $post = Yii::$app->request->post();

        $model['busTracking']->load($post);

        $transaction['busTracking'] = BusTracking::getDb()->beginTransaction();

        try {
            if ($model['busTracking']->isNewRecord) {}
            if (!$model['busTracking']->save()) {
                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
            }
            
            $transaction['busTracking']->commit();
        } catch (\Exception $e) {
            $error = true;
            $transaction['busTracking']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['busTracking']->rollBack();
        }

        if ($error)
            if (isset($model['busTracking']->errors))
                return [
                    "status" => "fail",
                    "data" => $model['busTracking']->errors
                ];
            else
                return [
                    "status" => "error",
                    "message" => "We've faced a problem updating the bus, please contact us for assistance."
                ];
        else
            return [
                "status" => "success",
                "data" => "Bus Tracking updated successfully",
            ];
    }

    protected function findModel($id)
    {
        if (($model = BusTracking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}