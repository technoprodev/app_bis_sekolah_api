<?php

namespace app_bis_sekolah_api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use app_bis_sekolah_admin\models\Bus;
use yii\widgets\ActiveForm;

class BusController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter for cors to work
        unset($behaviors['authenticator']);

        // Allow XHR Requests from our different subdomains and dev machines
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        // Bearer Auth checks for Authorize: Bearer <Token> header to login the user
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

 
    public function actionIndex()  {
        $post = Yii::$app->request->post();
        if (!$post) {
               return [
                "status" => "success",
                 "data" => Bus::find()->asArray()->all(),
             ];
        }  else {
            $id = $post['Bus']['id'];
            $rows = Yii::$app->dba->createCommand('SELECT * FROM bus WHERE id=:id')
               ->bindValue(':id', $id)
               ->queryAll();

            if($rows) {
                return [
                    // "status" => "succes",
                    // "data" => $post,
                    "status" => "success",
                    "data" => $rows,
                ];  
            } else {
                return [
                    "status" => "failed",
                    "data" => $post,
                ]; 
                 
            }
        }    
    }

    public function actionCreate()
    {
        $error = false;

        $model['bus'] = isset($id) ? $this->findModel($id) : new Bus();

        $post = Yii::$app->request->post();
        //$post.['id'];//aku tambah

        $model['bus']->load($post);

        $transaction['bus'] = Bus::getDb()->beginTransaction();

        try {
            if ($model['bus']->isNewRecord) {}
            if (!$model['bus']->save()) {
                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
            }
            
            $transaction['bus']->commit();
        } catch (\Exception $e) {
            $error = true;
            $transaction['bus']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['bus']->rollBack();
        }

        if ($error)
            if (isset($model['bus']->errors))
                return [
                    "status" => "fail",
                    "data" => $model['bus']->errors
                ];
            else
                return [
                    "status" => "error",
                    "message" => "We've faced a problem creating the bus, please contact us for assistance."
                ];
        else
            return [
                "status" => "success",
                "data" => "Bus created successfully",
            ];
    }

    public function actionUpdate($id)
    {
        $error = false;

        $model['bus'] = isset($id) ? $this->findModel($id) : new Bus();

        $post = Yii::$app->request->post();

        $model['bus']->load($post);

        $transaction['bus'] = Bus::getDb()->beginTransaction();

        try {
            if ($model['bus']->isNewRecord) {}
            if (!$model['bus']->save()) {
                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
            }
            
            $transaction['bus']->commit();
        } catch (\Exception $e) {
            $error = true;
            $transaction['bus']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['bus']->rollBack();
        }

        if ($error)
            if (isset($model['bus']->errors))
                return [
                    "status" => "fail",
                    "data" => $model['bus']->errors
                ];
            else
                return [
                    "status" => "error",
                    "message" => "We've faced a problem updating the bus, please contact us for assistance."
                ];
        else
            return [
                "status" => "success",
                "data" => "Bus updated successfully",
            ];
    }

    protected function findModel($id)
    {
        if (($model = Bus::findOne($id)) !== null) {
            return $model;
        } else {
            return false;
            //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}