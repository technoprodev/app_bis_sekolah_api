<?php

namespace app_bis_sekolah_api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use app_bis_sekolah_admin\models\StudentAttendance; //Nama Model yang perlu di ketehui utk memangil api
use yii\widgets\ActiveForm;

class StudentAttendanceController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter for cors to work
        unset($behaviors['authenticator']);

        // Allow XHR Requests from our different subdomains and dev machines
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        // Bearer Auth checks for Authorize: Bearer <Token> header to login the user
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    // public function actionIndex($id = null)
    // {
    //     // view all data
    //     if (!$id)
    //         return [
    //             "status" => "success",
    //             "data" => StudentAttendance::find()->asArray()->all(),
    //         ];
        
    //     // view single data
    //     $model['studentAttendance'] = $this->findModel($id);
    //     return [
    //         "status" => "success",
    //         "data" => StudentAttendance::find($id)->asArray()->one(),
    //     ];
    // }

    public function actionIndex()  {
        $post = Yii::$app->request->post();
        if (!$post) {
               return [
                "status" => "success",
                 "data" => StudentAttendance::find()->asArray()->all(),
             ];
        }  else {
            
            $sqlStatme="SELECT * FROM student_attendance WHERE 1=1 ";
            $sqlCondition = " and id_student=:id_student";
            $sqlStatme=$sqlStatme.$sqlCondition; //Combine string + string
            $id_student = $post['StudentAttendance']['id_student'];
            $params = [':id_student' => $id_student]; 
            // $params = "["; 
            // $params .= ':id_student'; 
            //  $params .=  " => ";
            // $params .= $id_student; 
            // $params .= "]"; 



            // $rows = Yii::$app->dba->createCommand('SELECT * FROM student_attendance WHERE id_student=:id_student')
            // $rows = Yii::$app->dba->createCommand($sqlStatme)
            //    ->bindValue(':id_student', $id_student)
            //    ->queryAll();

            $rows = Yii::$app->dba->createCommand($sqlStatme, $params)
           ->queryAll();



            if($rows) {
                return [
                    // "status" => "succes",
                    // "data" => $post,
                    "status" => "success",
                    "data" => $rows,
                ];  
            } else {
                return [
                    "status" => "failed",
                    "data" => $post,
                ]; 
                 
            }
        }    
    }

    public function actionCreate()
    {
        $error = false;

        $model['studentAttendance'] = isset($id) ? $this->findModel($id) : new StudentAttendance();

        $post = Yii::$app->request->post();

        $model['studentAttendance']->load($post);

        $transaction['studentAttendance'] = StudentAttendance::getDb()->beginTransaction();

        try {
            if ($model['studentAttendance']->isNewRecord) {}
            if (!$model['studentAttendance']->save()) {
                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
            }
            
            $transaction['studentAttendance']->commit();
        } catch (\Exception $e) {
            $error = true;
            $transaction['studentAttendance']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['studentAttendance']->rollBack();
        }

        if ($error)
            if (isset($model['studentAttendance']->errors))
                return [
                    "status" => "fail",
                    "data" => $model['studentAttendance']->errors
                ];
            else
                return [
                    "status" => "error",
                    "message" => "We've faced a problem creating the bus, please contact us for assistance."
                ];
        else
            return [
                "status" => "success",
                "data" => "Bus Tracking created successfully",
            ];
    }

    public function actionUpdate($id)
    {
        $error = false;

        $model['studentAttendance'] = isset($id) ? $this->findModel($id) : new StudentAttendance();

        $post = Yii::$app->request->post();

        $model['studentAttendance']->load($post);

        $transaction['studentAttendance'] = StudentAttendance::getDb()->beginTransaction();

        try {
            if ($model['studentAttendance']->isNewRecord) {}
            if (!$model['studentAttendance']->save()) {
                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
            }
            
            $transaction['studentAttendance']->commit();
        } catch (\Exception $e) {
            $error = true;
            $transaction['studentAttendance']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['studentAttendance']->rollBack();
        }

        if ($error)
            if (isset($model['studentAttendance']->errors))
                return [
                    "status" => "fail",
                    "data" => $model['studentAttendance']->errors
                ];
            else
                return [
                    "status" => "error",
                    "message" => "We've faced a problem updating the bus, please contact us for assistance."
                ];
        else
            return [
                "status" => "success",
                "data" => "Bus Tracking updated successfully",
            ];
    }

    protected function findModel($id)
    {
        if (($model = StudentAttendance::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}